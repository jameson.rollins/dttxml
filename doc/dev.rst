Development
################################################################################
.. _development:

Testing with py.test
================================================================================

.. py:module:: conftest


py.test fixtures and setup
--------------------------------------------------------------------------------


.. autofunction:: plot


.. autofunction:: tpath_preclear


.. autofunction:: tpath


.. autofunction:: tpath_join


.. autofunction:: fpath


.. autofunction:: fpath_join


.. autofunction:: closefigs


.. autofunction:: test_trigger


.. autofunction:: ic


.. autofunction:: pprint

py.test setup hooks
--------------------------------------------------------------------------------

.. autofunction:: pytest_addoption


py.test helper functions
--------------------------------------------------------------------------------


.. autofunction:: tpath_raw_make


.. autofunction:: fpath_raw_make


.. autofunction:: relfile
 
 
.. autofunction:: relfile_test



Git setup for development
================================================================================


PyPI Releases
================================================================================

versioning
--------------------------------------------------------------------------------

