
API
################################################################################
.. _API:

.. py:module:: dttxml

High Level DTT file usage
================================================================================

.. autoclass:: DiagAccess


.. autoclass:: dttxml.access.DiagFreqMeasurementHolder


.. autoclass:: dttxml.access.DiagCoherenceHolder


.. autoclass:: dttxml.access.DiagCSDHolder


.. autoclass:: dttxml.access.DiagASDHolder


.. autoclass:: dttxml.access.DiagXferHolder


.. autoclass:: dttxml.access.DiagXferViaHolder


.. autoclass:: dttxml.access.DiagSineResponseCoefficients


.. autoclass:: dttxml.access.DiagHarmonicResponseCoefficients


.. autoclass:: dttxml.access.DiagMeasurementHolder



DTT to python dictionary or hdf
================================================================================

.. autofunction:: dtt_read


Low Level DTT access (accesses references)
================================================================================

.. autofunction:: dtt2bunch


